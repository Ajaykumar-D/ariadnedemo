from ariadne import ObjectType
# from ariadne import QueryType
import config

# Create type instance for Query type defined in our schema...
# query = QueryType()
query = ObjectType("Query")
user = ObjectType("User")
user.set_alias("username", "fullName")


# ...and assign our resolver function to its "hello" field.
@query.field("hello")
def resolve_hello(_, info):
    request = info.context["request"]
    user_agent = request.headers.get("user-agent", "guest")
    return "Hello ajayy, %s!" % user_agent


@query.field("user")
def resolve_user(_, info):
    return {
        "first_name": "John",
        "last_name": "Lennon",
        "email": "ajay@abcd.lcl",
        "is_administrator": True,
    }


@user.field("username")
def resolve_username(obj, *_):
    return f"{obj['first_name']} {obj['last_name']}"


@query.field("holidays")
def resolve_holidays(*_, year=None):
    if year:
        return ["March 22", "April 31", "Nov 05"]
    return []


@query.field("getvalues")
def resolve_getvalues(_, info):
    return {
        "wait_time": config.wait_time,
        "subscription_count": int(config.subscription_count),
        "target_sub_count": int(config.target_sub_count),
    }
