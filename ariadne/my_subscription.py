import asyncio
import time
from ariadne import SubscriptionType
import config

subscription = SubscriptionType()


@subscription.source("counter")
async def counter_generator(obj, info):
    if config.subscription_count == 0:
        config.first_sub_time = time.time()

    config.subscription_count += 1
    print(f"{config.target_sub_count} --- Subs: {config.subscription_count}")
    if config.subscription_count == config.target_sub_count:
        total_time = time.time() - config.first_sub_time
        print(f"""All {config.target_sub_count} subs are active.
            Time taken:{total_time:.2f} s""")

    for i in range(50):
        await asyncio.sleep(config.wait_time)
        yield i


@subscription.field("counter")
def counter_resolver(count, info):
    return count + 1

# subscription.set_field("counter", counter_resolver)
# subscription.set_source("counter", counter_generator)
