from my_query import query, user
from my_mutation import mutation
from my_subscription import subscription
from ariadne import make_executable_schema, gql
from ariadne.asgi import GraphQL
from fastapi import FastAPI
import uvicorn
from starlette.middleware.cors import CORSMiddleware


TYPE_DEFS = gql(
    """
    type Query {
        hello: String!
        user: User
        holidays(year: Int): [String]!
        getvalues: constvalue
    }
    type constvalue {
        wait_time: Float
        subscription_count: Int
        target_sub_count: Int
    }
    type User {
        username: String!
    }
    type Mutation {
        login(username: String!, password: String!): LoginResult!
        logout: Boolean!
        setvalues(count: Int!): Int!
    }
    type LoginResult {
        status: Boolean!
    }
    type Subscription {
        counter: Int!
    }
"""
)

schema = make_executable_schema(TYPE_DEFS, query, user, mutation, subscription)
graphql_handler = GraphQL(schema, debug=True)
app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)
app.mount("/", graphql_handler)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8001)
# routes = [
#     Route("/db", endpoint=graphql_handler),
#     WebSocketRoute("/socket", endpoint=graphql_handler),
# ]


# middleware = [
#     Middleware(
#         CORSMiddleware,
#         allow_origins=["*"],
#         allow_credentials=True,
#         allow_methods=["*"],
#         allow_headers=["*"]
#     )
# ]

# app = Starlette(routes=routes, middleware=middleware, debug=False)
