from ariadne import MutationType
import config

mutation = MutationType()


def resolve_login(_, info, username, password):
    request = info.context["request"]
    print(request)
    # user = auth.authenticate(username, password)
    if username == "ajay":
        # auth.login(request, user)
        return {"status": True}
    return False


@mutation.field("logout")
def resolve_logout(_, info):
    request = info.context["request"]
    print(request)
    # if request.user.is_authenticated:
    # auth.logout(request)
    # return True
    return True


@mutation.field("setvalues")
def resolve_setvalues(_, info, count):
    config.target_sub_count = count
    return count


mutation.set_field("login", resolve_login)
# mutation.set_field("logout", resolve_logout)
