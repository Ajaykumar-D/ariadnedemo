import { gql, useSubscription } from "@apollo/client"
import QueryComp from "./query";

const MESSAGE_SUBSCRIPTION = gql`
    subscription {
        counter
    }
`
const Dash = () => {
    // const { loading, error, data } = useSubscription(MESSAGE_SUBSCRIPTION);
    // console.log('--------------', data);
    
    // if (loading) return <p>Loading...</p>
    // if (error) return <p>Error: {error.message}</p>

    return (
        <div>
            <h1>Query</h1>
            <QueryComp />
            {/* <h1>Messages:</h1>
            <div>
                {data.counter && 
                    <p>{data.counter}</p>
                }
            </div> */}
        </div>
    )
}

export default Dash;