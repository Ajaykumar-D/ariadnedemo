import './App.css';
import { ApolloProvider } from '@apollo/client';
import { client } from './WebsocketConnection';
import Dash from './dash';

function App() {
  return (
    <div className="App">
     Data to fetch goes here
     <Dash />
    </div>
  );
}

const AppWithApollo = () => (
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>
)
export default AppWithApollo;
