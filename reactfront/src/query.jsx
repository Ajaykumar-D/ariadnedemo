import { gql, useQuery } from "@apollo/client"

const GET_DATA = gql`
        user {
           username
        }
`
const QueryComp = () => {
    const { loading, error, data } = useQuery(GET_DATA);

    console.log('dddddddddddddd', data);

    // if (loading) return <p>Loading...</p>
    // if (error) return <p>Error: {error.message}</p>

    return (
        <div>
            Query data:
            {data && 
                {data}
            }
        </div>
    )
}

export default QueryComp