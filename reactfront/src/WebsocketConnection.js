import { ApolloClient, InMemoryCache, createHttpLink, split } from "@apollo/client"
import { WebSocketLink } from '@apollo/client/link/ws';
import { getMainDefinition } from "@apollo/client/utilities"

const httpLink = createHttpLink({
    uri: "http://0.0.0.0:8001/",
})

const wsLink = new WebSocketLink({
    uri: "ws://0.0.0.0:8001/",
    options: {
        reconnect: true
    }
})

const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return definition.kind = "OperationDefinition" && definition.operation === "subscription";
    },
    wsLink,
    httpLink
)

export const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache(),
})
