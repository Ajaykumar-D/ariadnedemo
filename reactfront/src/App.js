// src/App.js
import React, { useState, useEffect, useRef, useCallback } from 'react';
import { ApolloProvider, gql, useMutation } from '@apollo/client';
import createApolloClient from './apolloClient';
import SubscriptionComponent from './SubscriptionComponent';
import QueryMutationComponent from './QueryMutationComponent';

// const SET_EXPECTED_SUBS = gql`
//   mutation setExpectedSubs($count: Int!) {
//     setExpectedSubs(count: $count)
//   }
// `;
const SET_EXPECTED_SUBS = gql`
  mutation login($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      status
    }
  }
`;


const App = () => {
  const [selectedBackend, setSelectedBackend] = useState('strawberry');
  const [numSubscriptions, setNumSubscriptions] = useState(100);
  const [clients, setClients] = useState([]);
  const setExpectedSubsCalled = useRef(false);
  const subscriptionRefs = useRef([]);

  const backends = {
    ariadne: {
      httpUri: 'http://0.0.0.0:8001/',
      wsUri: 'ws://0.0.0.0:8001/',
    },
    strawberry: {
      httpUri: 'http://0.0.0.0:8002/',
      wsUri: 'ws://0.0.0.0:8002/',
    },
  };

  const handleBackendChange = (event) => {
    stopAllSubscriptions();
    setSelectedBackend(event.target.value);
    setExpectedSubsCalled.current = false; // Reset flag when backend changes
  };

  const handleNumSubscriptionsChange = (event) => {
    stopAllSubscriptions();
    setNumSubscriptions(Number(event.target.value));
    setExpectedSubsCalled.current = false;  // Reset flag when number of subscriptions changes
  };

  const createClients = useCallback(() => {
    return [...Array(numSubscriptions)].map(() =>
      createApolloClient(backends[selectedBackend].httpUri, backends[selectedBackend].wsUri, true)
    );
  }, [numSubscriptions, selectedBackend]);

  useEffect(() => {
    setClients(createClients());
  }, [createClients]);

  const client = createApolloClient(
    backends[selectedBackend].httpUri,
    backends[selectedBackend].wsUri,
    true
  );

  const [setExpectedSubs] = useMutation(SET_EXPECTED_SUBS, { client });

  useEffect(() => {
    if (!setExpectedSubsCalled.current && numSubscriptions > 0) {
      setExpectedSubs({ variables: { username: "ajay", password: "fdsfdsa" } });
      setExpectedSubsCalled.current = true;
    }
  }, [numSubscriptions, selectedBackend, setExpectedSubs]);

  const handleStopSubscriptions = () => {
    stopAllSubscriptions();
  };

  const stopAllSubscriptions = () => {
    subscriptionRefs.current.forEach(ref => ref?.stop());
    subscriptionRefs.current = [];
    clients.forEach(client => {
      client.wsClient.close(false, false);
      client.wsClient.reconnect = false; // Prevent reconnections
    });
  };

  return (
    <ApolloProvider client={client}>
      <div className="App">
        <label htmlFor="backend-select">Choose a backend:</label>
        <select
          id="backend-select"
          value={selectedBackend}
          onChange={handleBackendChange}
        >
          <option value="ariadne">Ariadne</option>
          <option value="strawberry">Strawberry</option>
        </select>
        <label htmlFor="num-subscriptions">Number of Subscriptions:</label>
        <input
          type="number"
          id="num-subscriptions"
          value={numSubscriptions}
          onChange={handleNumSubscriptionsChange}
          min="1"
          style={{ maxWidth: "40px" }}
        />
        <QueryMutationComponent />
        <button onClick={handleStopSubscriptions}>Stop Subscriptions</button>
        <div id="subs" style={{ display: "ruby", width: "100vw" }}>
          { clients.map((client, index) => (
            <SubscriptionComponent
              key={index}
              client={client}
              ref={el => subscriptionRefs.current[index] = el}
            />
          ))}
        </div>
      </div>
    </ApolloProvider>
  );
};

export default App;
