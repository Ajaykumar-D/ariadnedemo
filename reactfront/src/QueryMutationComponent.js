// src/QueryMutationComponent.js
import React, { useState } from 'react';
import { gql, useApolloClient } from '@apollo/client';

const QueryMutationComponent = () => {
  const client = useApolloClient();
  const [query, setQuery] = useState('query{hello}');
  const [variables, setVariables] = useState('{}');
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);

  const handleQueryChange = (event) => {
    setQuery(event.target.value);
  };

  const handleVariablesChange = (event) => {
    setVariables(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const parsedVariables = JSON.parse(variables);
      const trimmedQuery = query.trim();
      let result;

      if (trimmedQuery.startsWith('mutation')) {
        result = await client.mutate({
          mutation: gql`${trimmedQuery}`,
          variables: parsedVariables,
        });
      } else {
        result = await client.query({
          query: gql`${trimmedQuery}`,
          variables: parsedVariables,
          fetchPolicy: 'no-cache', // Ensure fresh data is fetched
        });
      }

      setResponse(result.data);
      setError(null);
    } catch (err) {
      setError(err.message);
      setResponse(null);
    }
  };

  return (
    <div style={{display: "inline-flex"}}>
      <form onSubmit={handleSubmit} style={{display: "contents"}}>
        <div>
          <textarea
            value={query}
            onChange={handleQueryChange}
            placeholder="Enter your query or mutation here"
            rows="5"
            cols="20"
          />
        </div>
        <div>
          <textarea
            value={variables}
            onChange={handleVariablesChange}
            placeholder="Enter variables as JSON"
            rows="5"
            cols="20"
          />
        </div>
        <div>
          <button type="submit">Send</button>
        </div>
      </form>
      {response && (
        <div>
          <h5>Response:</h5>
          <pre>{JSON.stringify(response, null, 2)}</pre>
        </div>
      )}
      {error && (
        <div>
          <h3>Error:</h3>
          <pre>{error}</pre>
        </div>
      )}
    </div>
  );
};

export default QueryMutationComponent;
