"""
This creates the schema of project
"""

import typing
import strawberry
import random
import asyncio
from typing import AsyncGenerator


@strawberry.type
class Book:
    title: str
    author: str


books = [
    Book(
        title="My Title",
        author="F. Scott Fitzgerald",
    ),
]


def add_books(title, author):
    new_book = Book(title=title, author=author)
    books.append(new_book)
    return new_book


def get_books():
    return books


@strawberry.type
class Query:
    books: typing.List[Book] = strawberry.field(resolver=get_books)

    @strawberry.field
    def random_num(self) -> int:
        return random.randint(1, 1000)


@strawberry.type
class Mutation:
    @strawberry.mutation
    def add_book(self, title: str, author: str) -> Book:
        return add_books(title, author)

    @strawberry.mutation
    def restart(self) -> None:
        print("Restarting...")


@strawberry.type
class Subscription:
    @strawberry.subscription
    async def count(self, target: int = 100) -> AsyncGenerator[int, None]:
        try:
            for i in range(target):
                yield i
                await asyncio.sleep(0.5)
        except:
            print("Errror")


schema = strawberry.Schema(query=Query, mutation=Mutation, subscription=Subscription)
