"""
This creates the schema of project
"""

import typing
import strawberry
import random
import time
import asyncio
from typing import AsyncGenerator
from fastapi import FastAPI
import uvicorn
from strawberry.asgi import GraphQL
from starlette.middleware.cors import CORSMiddleware


wait_time = 0.1
subscription_count = 0
first_sub_time = 0
target_sub_count = 100
total_time = 0


@strawberry.type
class getvaluestype:
    wait_time: float
    subscription_count: int
    first_sub_time: float
    target_sub_count: int
    total_time: float


@strawberry.type
class statust:
    status: bool


@strawberry.type
class Book:
    title: str
    author: str


books = [
    Book(
        title="My Title",
        author="F. Scott Fitzgerald",
    ),
]


def add_books(title, author):
    new_book = Book(title=title, author=author)
    books.append(new_book)
    return new_book


def get_books():
    return books


@strawberry.type
class Query:
    books: typing.List[Book] = strawberry.field(resolver=get_books)

    @strawberry.field
    def random_num(self) -> int:
        return random.randint(1, 1000)

    @strawberry.field
    def get_values(self) -> getvaluestype:
        global wait_time, subscription_count, first_sub_time, target_sub_count, total_time
        return getvaluestype(
            wait_time=wait_time,
            subscription_count=subscription_count,
            first_sub_time=first_sub_time,
            target_sub_count=target_sub_count,
            total_time=total_time,
        )


@strawberry.type
class Mutation:
    @strawberry.mutation
    def add_book(self, title: str, author: str) -> Book:
        return add_books(title, author)

    @strawberry.mutation
    def login(self, username: str, password: str) -> statust:
        if username == "ajay":
            return statust(status=True)
        else:
            return statust(status=False)

    @strawberry.mutation
    def set_values(self, count: int) -> int:
        global subscription_count, target_sub_count, total_time
        total_time = 0  # Reset total time
        subscription_count = count  # Reset subscription count
        return count


@strawberry.type
class Subscription:
    @strawberry.subscription
    async def counter(self) -> AsyncGenerator[int, None]:
        global subscription_count, first_sub_time, target_sub_count, total_time
        if subscription_count == 0:
            first_sub_time = time.time()
        subscription_count += 1
        print(f"{target_sub_count} --- Subscription: {subscription_count}")
        if subscription_count == target_sub_count:
            total_time = time.time() - first_sub_time
            print(
                f"All {target_sub_count} subscriptions are active. Time taken: {total_time:.2f} seconds"
            )

        try:
            for i in range(50):
                await asyncio.sleep(0.1)
                yield i
        except:
            print("Errror")


schema = strawberry.Schema(query=Query, mutation=Mutation, subscription=Subscription)
graphql_app = GraphQL(schema, debug=True)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/", graphql_app)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8002)
